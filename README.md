## Catalog Entities

Simple projet (sans code !) regroupant des exemples d'[__Entity files__](https://15m2.gitlab.io/backstage/decouverte/architecture/datamodel.html) de [Backstage](https://backstage.io/).

Permet d'alimenter toute instance d'application Backstage dont la configuration permet de se connecter à un référentiel __GitLab__. 


